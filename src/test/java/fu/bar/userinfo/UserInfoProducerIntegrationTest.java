package fu.bar.userinfo;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;

import fu.bar.userinfo.dto.Post;
import fu.bar.userinfo.dto.UserInfo;
import fu.bar.userinfo.restconsumer.RestConsumer;

public class UserInfoProducerIntegrationTest {

    private final RestConsumer restConsumer = new RestConsumer();
    private final UserInfoProducer producer = new UserInfoProducer(restConsumer);

    @Test
    public void testConsumeRestApi() {
        UserInfo user = producer.user(1L).block();

        assertThat(user.getName()).isEqualTo("Leanne Graham");
        assertThat(user.getUsername()).isEqualTo("Bret");
        assertThat(user.getEmail()).isEqualTo("Sincere@april.biz");

        List<Post> posts = user.getPosts();
        assertThat(posts).hasSize(10);

        assertThat(posts.get(0).getId()).isEqualTo(1L);
        assertThat(posts.get(0).getTitle()).startsWith("sunt aut facere");

        assertThat(posts.get(1).getId()).isEqualTo(2L);
        assertThat(posts.get(1).getTitle()).startsWith("qui est esse");
    }

}
