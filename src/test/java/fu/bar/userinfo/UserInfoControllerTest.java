package fu.bar.userinfo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWebTestClient
public class UserInfoControllerTest {

    @Autowired
    private WebTestClient webClient;

    @Test
    public void testGetUserInfo() {
        webClient.get().uri("/users/1")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectBody()
                .jsonPath("$.name").isEqualTo("Leanne Graham")
                .jsonPath("$.username").isEqualTo("Bret")
                .jsonPath("$.email").isEqualTo("Sincere@april.biz")
                .jsonPath("$.posts").isArray()
                .jsonPath("$.posts[0].id").isEqualTo(1L)
                .jsonPath("$.posts[0].title").value(title -> {
                    assertThat(title.toString()).startsWith("sunt aut facere");
                });
    }

}
