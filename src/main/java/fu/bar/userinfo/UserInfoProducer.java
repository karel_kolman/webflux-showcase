package fu.bar.userinfo;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fu.bar.userinfo.dto.ImmutablePost;
import fu.bar.userinfo.dto.ImmutableUserInfo;
import fu.bar.userinfo.dto.Post;
import fu.bar.userinfo.dto.UserInfo;
import fu.bar.userinfo.restconsumer.RestConsumer;
import fu.bar.userinfo.restconsumer.dto.RestPost;
import fu.bar.userinfo.restconsumer.dto.RestUser;
import reactor.core.publisher.Mono;

@Component
public class UserInfoProducer {

    private final RestConsumer restConsumer;

    @Autowired
    public UserInfoProducer(RestConsumer restConsumer) {
        this.restConsumer = restConsumer;
    }

    /**
     * Produces {@link UserInfo} for the given user id.
     * 
     * @param userId
     * @return
     */
    public Mono<UserInfo> user(Long userId) {
        Mono<RestUser> user = restConsumer.user(userId);
        Mono<List<RestPost>> posts = restConsumer.posts(userId).collectList();
        return Mono.zip(user, posts, UserInfoProducer::mapUser);
    }

    /**
     * For zipping RestUser & List<Post> monos.
     * 
     * @param user
     * @param posts
     * @return
     */
    private static UserInfo mapUser(RestUser user, List<RestPost> restPosts) {
        List<Post> posts = restPosts.stream()
                .map(post -> mapPost(post))
                .collect(toList());

        return ImmutableUserInfo.builder()
                .name(user.getName())
                .username(user.getUsername())
                .email(user.getEmail())
                .posts(posts)
                .build();
    }

    private static Post mapPost(RestPost post) {
        return ImmutablePost.builder()
                .id(post.getId())
                .title(post.getTitle())
                .build();
    }

}
