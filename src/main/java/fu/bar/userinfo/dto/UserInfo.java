package fu.bar.userinfo.dto;

import java.util.List;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImmutableUserInfo.class)
@JsonDeserialize(as = ImmutableUserInfo.class)
public abstract class UserInfo {

    public abstract String getName();
    public abstract String getUsername();
    public abstract String getEmail();
    public abstract List<Post> getPosts();

}
