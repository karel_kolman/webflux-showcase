package fu.bar.userinfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import fu.bar.userinfo.dto.UserInfo;
import reactor.core.publisher.Mono;

@RestController
public class UserInfoController {

    private final UserInfoProducer userInfoProducer;

    @Autowired
    public UserInfoController(UserInfoProducer userInfoProducer) {
        this.userInfoProducer = userInfoProducer;
    }

    @GetMapping("/users/{userId}")
    public Mono<UserInfo> getUser(@PathVariable Long userId) {
        return userInfoProducer.user(userId);
    }

}
