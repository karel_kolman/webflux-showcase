package fu.bar.userinfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * WebFlux rest application.
 *
 */
@SpringBootApplication
public class UserInfoApp {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(UserInfoApp.class, args);
    }

}