package fu.bar.userinfo.restconsumer;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import fu.bar.userinfo.restconsumer.dto.RestPost;
import fu.bar.userinfo.restconsumer.dto.RestUser;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class RestConsumer {

    private static final String REST_API_ENDPOINT = "https://jsonplaceholder.typicode.com/";

    private final WebClient client = WebClient.create(REST_API_ENDPOINT);

    /**
     * Fetches user data from rest api.
     * 
     * @param userId
     * @return
     */
    public Mono<RestUser> user(Long userId) {
        Mono<RestUser> user = client
                .get()
                .uri("/users/{id}", userId)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(RestUser.class);
        return user;
    }

    /**
     * Fetches user posts from rest api.
     * 
     * @param userId
     * @return
     */
    public Flux<RestPost> posts(Long userId) {
        return client
                .get()
                .uri("/posts?userId={id}", userId)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(RestPost.class);
    }

}
