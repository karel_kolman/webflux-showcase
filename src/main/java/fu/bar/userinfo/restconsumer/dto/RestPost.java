package fu.bar.userinfo.restconsumer.dto;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImmutableRestPost.class)
@JsonDeserialize(as = ImmutableRestPost.class)
public abstract class RestPost {

    public abstract Long getId();
    public abstract Long getUserId();
    public abstract String getTitle();
    public abstract String getBody();

}
