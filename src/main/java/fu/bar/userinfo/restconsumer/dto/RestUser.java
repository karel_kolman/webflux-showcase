package fu.bar.userinfo.restconsumer.dto;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImmutableRestUser.class)
@JsonDeserialize(as = ImmutableRestUser.class)
public abstract class RestUser {

    public abstract Long getId();
    public abstract String getName();
    public abstract String getUsername();
    public abstract String getEmail();
    public abstract RestAddress getAddress();

}
