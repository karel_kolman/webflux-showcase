package fu.bar.userinfo.restconsumer.dto;

import org.immutables.value.Value;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Value.Immutable
@JsonSerialize(as = ImmutableRestAddress.class)
@JsonDeserialize(as = ImmutableRestAddress.class)
public abstract class RestAddress {

    public abstract String getStreet();
    public abstract String getSuite();
    public abstract String getCity();
    public abstract String getZipcode();

}
