# WebClient/WebFlux showcase

## Code
- `RestConsumer.java` consumes external REST apis via Spring's reactive WebClient and exposes them as Mono/Flux structures
- `UserInfoProducer.java` performs user data extraction/zipping in reactive fashion.
- `UserInfoApp.java` exposes the service as a REST api.

## Testing & running
To launch webflux app run `./gradlew bootRun`.

Then visit http://localhost:8080/users/1
